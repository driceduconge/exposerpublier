<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="style_smol.css" type="text/css">
  <title>ExposerPublier</title>
</head>

<body>
  <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $error = false;
      if(($_POST['editions'])== []) {
        echo "<script>alert(\"vous n\'avez sélectionné aucun produit !\")</script>";
        $error = true;
      }
      else{
        $liste_produits = $_POST['editions'];
        $prods = "";
        foreach ($liste_produits as $produit){
          $prods .= "\n".test_input($produit);
        }
      }

      if(empty($_POST['email'])) {
        echo "<script>alert(\"Renseignez votre email !\")</script>";
        $error = true;
      }
      else{
        $email = $_POST['email'];
      }

      if(empty($_POST['nom'])) {
        echo "<script>alert(\"Renseignez votre nom !\")</script>";
        $error = true;
      }
      else{
        $nom = test_input($_POST['nom']);
      }

      if(empty($_POST['prenom'])) {
        echo "<script>alert(\"Renseignez votre prenom !\")</script>";
        $error = true;
      }
      else{
        $prenom = test_input($_POST['prenom']);
      }

      if(empty($_POST['adresse'])) {
        echo "<script>alert(\"Renseignez votre adresse !\")</script>";
        $error = true;
      }
      else{
        $adresse = test_input($_POST['adresse']);
      }

      if(empty($_POST['ville'])) {
        echo "<script>alert(\"Renseignez votre ville !\")</script>";
        $error = true;
      }
      else{
        $ville = test_input($_POST['ville']);
      }

      if(empty($_POST['codepostal'])) {
        echo "<script>alert(\"Renseignez votre CP !\")</script>";
        $error = true;
      }
      else{
        $code_postal = test_input($_POST['codepostal']);
      }

      $etat = $_POST['etat'];

      if(empty($_POST['pays'])) {
        echo "<script>alert(\"Renseignez votre PAYSSSS !\")</script>";
        $error = true;
      }
      else{
        $pays = test_input($_POST['pays']);
      }
      if($error == false){
        $mail = "-----------------------------\n\nListe des produits :\n\n".$prods."\n\n-----------------------------\n\n"."Total\n\n-----------------------------\n\n"."Coordonées\n\n-----------------------------\n\n".$email."\n\n".$prenom." ".$nom."\n\n".$adresse."\n".$code_postal." ".$ville."\n".$pays."\n\n-----------------------------";
        $object = "Commande : ".$prenom." ".$nom;
        mail("contact@dddoss.eu",$object,$mail);
        mail($email,"Récapitulatif de votre commande auprès d'ExposerPublier",$mail."\n\n\n Nous vous recontacterons dans les 5 jours ouvrés, merci de nous avoir passé commande.\n\nExposerPublier\n5 rue de la Croix-Faubin\n75011 PARIS");

      }

    }
    function test_input($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
  ?>
  <pre style="width:100%;">
 <a href="index.html" style="white-space:pre;"><span class="retourligne">                      </span>ExposerPublier<span class="retourligne">      <br>                             </span>✝<br><span class="retourligne">                      </span>[ 2014 · 2022 ]</a>

 ↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔

                  Formulaire de commande

 ↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔


   Afin de valider votre commande, nous vous invitons
   à remplir le formulaire ci-dessous. Après réception
   de ce dernier nous vous recontacterons avec un lien
   de paiement prenant en compte les frais de port.

   Pour prendre plus en avant connaissance des éditions,
   nous vous invitons à consulter la section "Editions"
   de notre page d'accueil.


 .........................................................
 :                                                       :
 :    &Eacute;ditions                                           :
 :                                                       :
 :                                                       :
 :   <form style="display:inline;" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"><div class="checks"><input type="checkbox" name="amalgame" id="amalgame" value="amalgame"><span class="checkmark"> </span><label for="amalgame">Amalgame quadrichromie                 10 EUR</label></div>    :
 :                                                       :
 :   <div class="checks"><input type="checkbox" name="editions[]" id="beauxrestes" value="beauxrestes"><span class="checkmark"> </span><label for="beauxrestes">Beaux restes                            5 EUR</label></div>    :
 :                                                       :
 :   <div class="checks"><input type="checkbox" name="editions[]" id="exagraphie" value="exagraphie"><span class="checkmark"> </span><label for="exagraphie">Aurélie Pétrel. Exagraphie             40 EUR</label></div>    :
 :                                                       :
 :   <div class="checks"><input type="checkbox" name="editions[]" id="affichematrice" value="affichematrice"><span class="checkmark"> </span><label for="affichematrice">Affiche matrice                        20 EUR</label></div>    :
 :                                                       :
 :   <div class="checks"><input type="checkbox" name="editions[]" id="inventaire" value="inventaire"><span class="checkmark"> </span><label for="inventaire">Inventaire avant publication           10 EUR</label></div>    :
 :                                                       :
 :   <div class="checks"><input type="checkbox" name="editions[]" id="bisou" value="petit_bisou"><span class="checkmark"> </span><label for="bisou">Petit bisou, grand appartement         15 EUR</label></div>    :
 :                                                       :
 :   <div class="checks"><input type="checkbox" name="editions[]" id="derive" value="derive"><span class="checkmark"> </span><label for="derive">La technique de la dérive               2 EUR</label></div>    :
 :                                                       :
 :   <div class="checks"><input type="checkbox" name="editions[]" id="revueX" value="revueX"><span class="checkmark"> </span><label for="revueX">Re-vue nºX                              0 EUR</label></div>    :
 :                                                       :
 :   <div class="checks"><input type="checkbox" name="editions[]" id="exoagraphie" value="exoagraphie"><span class="checkmark"> </span><label for="exoagraphie">Ex(o/a)graphie                       1500 EUR</label></div>    :
 :                                                       :
 :   <div class="checks"><input type="checkbox" name="editions[]" id="revue0" value="revue0"><span class="checkmark"> </span><label for="revue0">Re-vue n°0                              0 EUR</label></div>    :
 :                                                       :
 :   <div class="checks"><input type="checkbox" name="editions[]" id="cartesmemoires" value="cartesmemoires"><span class="checkmark"> </span><label for="cartesmemoires">Carte(s) Mémoire(s)                    40 EUR</label></div>    :
 :                                                       :
 :   <div class="checks"><input type="checkbox" name="editions[]" id="ruines" value="ruines"><span class="checkmark"> </span><label for="ruines">Ruines du Soleil                       15 EUR</label></div>    :
 :                                                       :
 :   <div class="checks"><input type="checkbox" name="editions[]" id="exchange" value="exchange"><span class="checkmark"> </span><label for="exchange">Exchange Program #1                    10 EUR</label></div>    :
 :                                                       :
 :   <div class="checks"><input type="checkbox" name="editions[]" id="papierpeinture" value="papierpeinture"><span class="checkmark"> </span><label for="papierpeinture">Papier Peinture / Sample Book         300 EUR</label></div>    :
 :                                                       :
 :   <div class="checks"><input type="checkbox" name="editions[]" id="tac3" value="tac3"><span class="checkmark"> </span><label for="tac3">Table as a curator / Autour de la table<span class="retourligne" >          :   <br> :      </span>#3 Mixing console / Table de mixage    70 EUR</label></div>    :
 :                                                       :
 :   <div class="checks"><input type="checkbox" name="editions[]" id="tac2" value="tac2"><span class="checkmark"> </span><label for="tac2">Table as a curator / Autour de la table<span class="retourligne" >          :   <br> :      </span>#2 Printpress / Table de sérigraphie   15 EUR</label></div>    :
 :                                                       :
 :   <div class="checks"><input type="checkbox" name="editions[]" id="tac1" value="tac1"><span class="checkmark"> </span><label for="tac1">Table as a curator / Autour de la table<span class="retourligne" >          :   <br> :      </span>Nightstand / Table de chevet           10 EUR</label></div>    :
 :                                                       :
 :. . . . . . . . . . . . . . . . . . . . . . . . . . . .:
 :                                                       :
 :    Coordonnées                                        :
 :                                                       :
 :    E-MAIL                                             :
 :    <input class="textcoord"type="text" name="email" id="email" size="47">    :
 :                                                       :
 :    NOM                                                :
 :    <input class="textcoord"type="text" name="nom" id="nom" size="47">    :
 :                                                       :
 :    PR&Eacute;NOM                                             :
 :    <input class="textcoord"type="text" name="prenom" id="prenom" size="47">    :
 :                                                       :
 :    ADRESSE                                            :
 :    <input class="textcoord"type="text" name="adresse" id="adresse" size="47">    :
 :                                                       :
 :    VILLE                                              :
 :    <input class="textcoord"type="text" name="ville" id="ville" size="47">    :
 :                                                       :
 :    CODE POSTAL                                        :
 :    <input class="textcoord"type="text" name="codepostal" id="codepostal" size="47">    :
 :                                                       :
 :    ETAT                                               :
 :    <input class="textcoord"type="text" name="etat" id="etat" size="47">    :
 :                                                       :
 :    PAYS                                               :
 :    <input class="textcoord"type="text" name="pays" id="pays" size="47">    :
 :                                                       :
 :. . . . . . . . . . . . . . . . . . . . . . . . . . . .:
 :                                                       :
 :   En cliquant sur le bouton suivant, vous recevrez    :
 :   un e-mail de synthèse de commande. ExposerPublier   :
 :   recevra aussi votre bon de commande et vous         :
 :   et vous adressera alors dans les 5 jours ouvrés     :
 :   un e-mail de confirmation avec lien de paiement.    :
 :   En envoyant ce formulaire, vous acceptez les        :
 :   conditions générales de vente.                      :
 :                                                       :
 :                                                       :
 :                       <input type="submit" value="ENVOYER">                        :
 :                                                       :
 :. . . . . . . . . . . . . . . . . . . . . . . . . . . .:
 :                                                       :
 :                         MERCI !                       :

</form>

 ↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔

 <a href="credits.html">Crédits</a>                                            <a href="cgv.html">C.G.V.</a>


</body>
</html>
