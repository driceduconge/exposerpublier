var class_list = document.getElementsByClassName("s_section");
var section_list = document.getElementsByClassName("section");
var counter_slideshow = 0;

function display_click(id, link) {
  console.time();
  console.log(id);
  const el = document.getElementById(id);
  if (link.classList.contains("section")) {
    for (var i = 0; i < section_list.length; i++) {
      sect = section_list.item(i);
      console.log(sect.dataset.ref);
      console.log(document.getElementById(sect.dataset.ref).children);
      if (sect != link) {
        document.getElementById(sect.dataset.ref).style.display = "none";
        sect.style.textDecoration = "none";
      }
    }
  }
  if (el.style.display == "" || el.style.display == "none") {
    link.innerHTML = link.innerHTML.replace("&gt;&gt;", '&lt;&lt;');
    for (var i = 0; i < class_list.length; i++) {
      class_doc = class_list.item(i);
      if (!(class_doc.contains(el)) && !(id="memori")) class_doc.style.display = "none";
    }
    if (!(id="memori")) link.style.textDecoration = "underline";
    el.style.display = "inline";
  } else {
    el.style.display = "none";
    link.style.textDecoration = "none";
    link.innerHTML = link.innerHTML.replace("&lt;&lt;", ">>");
  }
  console.timeEnd();
  counter_slideshow = 0;
  counts = el.querySelector('.counting');
  console.log("test nombre : " + counts.parentNode.children.length - 4)
  counts.innerHTML = "1 / " + (counts.parentNode.children.length - 4);
}

// IMAGE SLIDESHOW

function next_image(link, value) {
  var parent = link.parentNode;
  console.log(parent);
  var children_list = parent.children;
  children_list[counter_slideshow].style.display = "none";
  console.log(children_list);
  counter_slideshow += value;
  (counter_slideshow == children_list.length - 4) ? counter_slideshow = 0: false;
  if (counter_slideshow < 0) counter_slideshow = counts.parentNode.children.length - 5;
  children_list[counter_slideshow].style.display = "block";
  counts = parent.querySelector('.counting');
  counts.innerHTML = (counter_slideshow + 1) + " / " + (counts.parentNode.children.length - 4);
}

// DISPLAY quantity



// INC AND DEC

function incdec_value(it, val){
  var sibling;
  (val < 0) ? sibling = it.nextElementSibling : sibling = it.previousElementSibling;
  var temp = parseInt(sibling.value);
  (temp == sibling.max && val == 1 || temp == 0 && val == -1) ? false : temp += val;
  sibling.value = temp;
  var par1 = it.parentNode;
  var par = par1.parentNode;
  par.dataset.quant = temp;
  if(temp>0){par.querySelector(":scope > input").checked = true;}else{par.querySelector(":scope > input").checked = false;}
}


// TOTAL CALCUL

function check_total(){
  let total = 0;
  var liste_items = document.getElementsByClassName("checks");
  for(var i = 0; i<liste_items.length;i++){
    it = liste_items.item(i);
    var temp = it.dataset.quant*it.dataset.preis;
      child = it.querySelector(":scope > .quantity_cont > .sous_total");
      console.log(it);
      console.log(child);
        console.log(child.innerHTML);
        console.log(temp);

        switch(temp.toString().length){
          case 1:
            if(child.innerHTML=="···0"){break;}
            (temp>0) ? child.innerHTML = "···"+temp : child.innerHTML = "····";
            break;
          case 2:
            console.log("2 chiffres :");
            displaynum = "··"+temp;
            console.log(displaynum);
            child.innerHTML = displaynum;
            break;
          case 3:
            child.innerHTML = "·"+temp;
            break;
          case 4:
            child.innerHTML = temp;
            break;
        }
            total = total + temp;
            var place = document.body.querySelector("#total");
            place.value = total;
    }

}


document.body.addEventListener("click", function(){check_total();},false);
