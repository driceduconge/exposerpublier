<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="style.css" type="text/css">
  <title>ExposerPublier</title>
</head>

<body>
  <pre style="width:100%;">
 <a href="index.html" style="white-space:pre;"><span class="retourligne">                     </span>Exposer✝Publier</a><pre class="s_section" ><span class="retourligne">      <br>                             </span>✝<br><span class="retourligne">                      </span>[ 2014 · 2022 ]</pre>

 ↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔

                  Formulaire de commande

 ↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔


   Afin de valider votre commande, nous vous invitons
   à remplir le formulaire ci-dessous. Après réception
   de ce dernier nous vous recontacterons avec un lien
   de paiement prenant en compte les frais de port.

   Veuillez renseigner tous les champs marqués d'un *.

   Pour prendre plus en avant connaissance des éditions,
   nous vous invitons à consulter la section "Editions"
   de notre page d'accueil.
<?php
     if ($_SERVER["REQUEST_METHOD"] == "POST") {
       $error = false;
       if(($_POST['editions'])== []) {
         echo "<script>alert(\"vous n\'avez sélectionné aucun produit !\")</script>";
         $error = true;
         die();
       }
       else{
         $liste_produits = $_POST['editions'];
         $qte_produit = $_POST['editionsqt'];
         $prods = "";
         for ($i = 0; $i<count($liste_produits);$i++){
           $prods .= "\n  "."Qté ".$qte_produit[$i]."  ".test_input($liste_produits[$i]);
         }
       }

       if(empty($_POST['email'])) {
         echo "<script>alert(\"Renseignez votre email !\")</script>";
         $error = true;
       }
       else{
         $email = $_POST['email'];
       }

       if(empty($_POST['nom'])) {
         echo "<script>alert(\"Renseignez votre nom !\")</script>";
         $error = true;
       }
       else{
         $nom = test_input($_POST['nom']);
       }

       if(empty($_POST['prenom'])) {
         echo "<script>alert(\"Renseignez votre prenom !\")</script>";
         $error = true;
       }
       else{
         $prenom = test_input($_POST['prenom']);
       }

       if(empty($_POST['adresse'])) {
         echo "<script>alert(\"Renseignez votre adresse !\")</script>";
         $error = true;
       }
       else{
         $adresse = test_input($_POST['adresse']);
       }

       if(empty($_POST['ville'])) {
         echo "<script>alert(\"Renseignez votre ville !\")</script>";
         $error = true;
       }
       else{
         $ville = test_input($_POST['ville']);
       }

       if(empty($_POST['codepostal'])) {
         echo "<script>alert(\"Renseignez votre CP !\")</script>";
         $error = true;
       }
       else{
         $code_postal = test_input($_POST['codepostal']);
       }

       $etat =  test_input($_POST['etat']);

       if(empty($_POST['pays'])) {
         echo "<script>alert(\"Renseignez votre PAYSSSS !\")</script>";
         $error = true;
       }
       else{
         $pays = test_input($_POST['pays']);
       }
       $total = $_POST['total'];
       if($error == false){
         $mail = "----------------------------------------------------------\n\n  Liste des produits :\n".$prods."\n\n----------------------------------------------------------\n\n"."  Total\n\n  ".$total." Euros\n\n"."\n\n----------------------------------------------------------\n\n"."  Coordonées\n\n----------------------------------------------------------\n\n  ".$email."\n\n  ".$prenom." ".$nom."\n\n  ".$adresse."\n  ".$code_postal." ".$ville."\n  ".$pays."\n\n----------------------------------------------------------";
         $object = "Commande : ".$prenom." ".$nom;
         //echo($mail);
         mail("contact@dddoss.eu",$object,$mail);
          echo("\n /=======================================================\ \n |                                                       |\n |  Un mail récapitulatif a été envoyé sur votre mail    |\n |  Vous le recevrez dans l'heure.                       |\n |                                                       |\n \\=======================================================/\n");
         mail($email,"Récapitulatif de votre commande auprès d'ExposerPublier","\n\n                           Merci !\n\n".$mail."\n\n\n  Nous vous recontacterons dans les 5 jours ouvrés.\n\n  ExposerPublier\n  5 rue de la Croix-Faubin\n  75011 PARIS");

       }

     }
     function test_input($data) {
       $data = trim($data);
       $data = stripslashes($data);
       $data = htmlspecialchars($data);
       return $data;
     }
   ?>

 :- - - - - - - - - - - - - - - - - - - - - - - - - - - -:
 :                                                       :
 :                        &Eacute;DITIONS                       :
 :. . . . . . . . . . . . . . . . . . . . . . . . . . . .:
 :                                                       :
 :   _________________________________________________   :
 :                                                       :
 :   <form style="display:inline;" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"><div class="checks" data-preis="10" data-quant="0"><input type="checkbox" class="checkinp" name="amalgame" id="amalgame" value="Amalgame quadrichromie (10 EUR)"><label for="amalgame">Amalgame quadrichromie</label>                              :<div class="quantity_cont"><br> :   Qté      <span class="dec" onclick="incdec_value(this,-1)">- </span><input name="editionsqt[]" type="number" min="0" max="9" value="0"><span class="inc" onclick="incdec_value(this,1)"> +</span>                          <span class="sous_total">····</span> EUR   :</div></div>
 :   _________________________________________________   :
 :                                                       :
 :   <div class="checks" data-preis="5" data-quant="0"><input type="checkbox" class="checkinp" name="editions[]" id="beauxrestes" value="Beaux restes (5 EUR)"><label  for="beauxrestes">Beaux restes</label>                                        :<div class="quantity_cont"><br> :   Qté      <span class="dec" onclick="incdec_value(this,-1)">- </span><input name="editionsqt[]" type="number" min="0" max="9" value="0"><span class="inc" onclick="incdec_value(this,1)"> +</span>                          <span class="sous_total">····</span> EUR   :</div></div>
 :   _________________________________________________   :
 :                                                       :
 :   <div class="checks" data-preis="40" data-quant="0"><input type="checkbox" class="checkinp" name="editions[]" id="exagraphiefr" value="Aurélie Pétrel. Exagraphie FR (40 EUR)"><label  for="exagraphie">Aurélie Pétrel. Exagraphie (FR)</label>                     :<div class="quantity_cont"><br> :   Qté      <span class="dec" onclick="incdec_value(this,-1)">- </span><input name="editionsqt[]" type="number" min="0" max="9" value="0"><span class="inc" onclick="incdec_value(this,1)"> +</span>                          <span class="sous_total">····</span> EUR   :</div></div>
 :   _________________________________________________   :
 :                                                       :
 :   <div class="checks" data-preis="40" data-quant="0"><input type="checkbox" class="checkinp" name="editions[]" id="exagraphieen" value="Aurélie Pétrel. Exagraphie EN (40 EUR)"><label  for="exagraphie">Aurélie Pétrel. Exagraphie (EN)</label>                     :<div class="quantity_cont"><br> :   Qté      <span class="dec" onclick="incdec_value(this,-1)">- </span><input name="editionsqt[]" type="number" min="0" max="9" value="0"><span class="inc" onclick="incdec_value(this,1)"> +</span>                          <span class="sous_total">····</span> EUR   :</div></div>
 :   _________________________________________________   :
 :                                                       :
 :   <div class="checks" data-preis="20" data-quant="0"><input type="checkbox" class="checkinp" name="editions[]" id="affichematrice" value="Affiche matrice (20 EUR)"><label  for="affichematrice">Affiche matrice</label>                                     :<div class="quantity_cont"><br> :   Qté      <span class="dec" onclick="incdec_value(this,-1)">- </span><input name="editionsqt[]" type="number" min="0" max="9" value="0"><span class="inc" onclick="incdec_value(this,1)"> +</span>                          <span class="sous_total">····</span> EUR   :</div></div>
 :   _________________________________________________   :
 :                                                       :
 :   <div class="checks" data-preis="10" data-quant="0"><input type="checkbox" class="checkinp" name="editions[]" id="inventaire" value="Inventaire avant publication (10 EUR)"><label  for="inventaire">Inventaire avant publication</label>                        :<div class="quantity_cont"><br> :   Qté      <span class="dec" onclick="incdec_value(this,-1)">- </span><input name="editionsqt[]" type="number" min="0" max="9" value="0"><span class="inc" onclick="incdec_value(this,1)"> +</span>                          <span class="sous_total">····</span> EUR   :</div></div>
 :   _________________________________________________   :
 :                                                       :
 :   <div class="checks" data-preis="15" data-quant="0"><input type="checkbox" class="checkinp" name="editions[]" id="bisou" value="Petit bisou, grand appartement (15 EUR)"><label  for="bisou">Petit bisou, grand appartement</label>                      :<div class="quantity_cont"><br> :   Qté      <span class="dec" onclick="incdec_value(this,-1)">- </span><input name="editionsqt[]" type="number" min="0" max="9" value="0"><span class="inc" onclick="incdec_value(this,1)"> +</span>                          <span class="sous_total">····</span> EUR   :</div></div>
 :   _________________________________________________   :
 :                                                       :
 :   <div class="checks" data-preis="2" data-quant="0"><input type="checkbox" class="checkinp" name="editions[]" id="derive" value="La technique de la dérive (2 EUR)"><label  for="derive">La technique de la dérive</label>                           :<div class="quantity_cont"><br> :   Qté      <span class="dec" onclick="incdec_value(this,-1)">- </span><input name="editionsqt[]" type="number" min="0" max="9" value="0"><span class="inc" onclick="incdec_value(this,1)"> +</span>                          <span class="sous_total">····</span> EUR   :</div></div>
 :   _________________________________________________   :
 :                                                       :
 :   <div class="checks" data-preis="0" data-quant="0"><input type="checkbox" class="checkinp" name="editions[]" id="revueX" value="Re-vue nºX (GRATUIT)"><label  for="revueX">Re-vue nºX</label>                                          :<div class="quantity_cont"><br> :   Qté      <span class="dec" onclick="incdec_value(this,-1)">- </span><input name="editionsqt[]" type="number" min="0" max="9" value="0"><span class="inc" onclick="incdec_value(this,1)"> +</span>                          <span class="sous_total">····</span> EUR   :</div></div>
 :   _________________________________________________   :
 :                                                       :
 :   <div class="checks" data-preis="1500" data-quant="0"><input type="checkbox" class="checkinp" name="editions[]" id="exoagraphie" value="Ex(o/a)graphie (1500 EUR)"><label  for="exoagraphie">Ex(o/a)graphie</label>                                   </label>   :<div class="quantity_cont"><br> :   Qté      <span class="dec" onclick="incdec_value(this,-1)">- </span><input name="editionsqt[]" type="number" min="0" max="3" value="0"><span class="inc" onclick="incdec_value(this,1)"> +</span>                          <span class="sous_total">····</span> EUR   :</div></div>
 :   _________________________________________________   :
 :                                                       :
 :   <div class="checks" data-preis="0" data-quant="0"><input type="checkbox" class="checkinp" name="editions[]" id="revue0" value="Re-vue n°0 (GRATUIT)"><label  for="revue0">Re-vue n°0</label>                                          :<div class="quantity_cont"><br> :   Qté      <span class="dec" onclick="incdec_value(this,-1)">- </span><input name="editionsqt[]" type="number" min="0" max="9" value="0"><span class="inc" onclick="incdec_value(this,1)"> +</span>                          <span class="sous_total">····</span> EUR   :</div></div>
 :   _________________________________________________   :
 :                                                       :
 :   <div class="checks" data-preis="40" data-quant="0"><input type="checkbox" class="checkinp" name="editions[]" id="cartesmemoires" value="Carte(s) Mémoire(s) (40 EUR)"><label  for="cartesmemoires">Carte(s) Mémoire(s)</label>                                 :<div class="quantity_cont"><br> :   Qté      <span class="dec" onclick="incdec_value(this,-1)">- </span><input name="editionsqt[]" type="number" min="0" max="9" value="0"><span class="inc" onclick="incdec_value(this,1)"> +</span>                          <span class="sous_total">····</span> EUR   :</div></div>
 :   _________________________________________________   :
 :                                                       :
 :   <div class="checks" data-preis="15" data-quant="0"><input type="checkbox" class="checkinp" name="editions[]" id="ruines" value="Ruines du Soleil (15 EUR)"><label  for="ruines">Ruines du Soleil</label>                                    :<div class="quantity_cont"><br> :   Qté      <span class="dec" onclick="incdec_value(this,-1)">- </span><input name="editionsqt[]" type="number" min="0" max="9" value="0"><span class="inc" onclick="incdec_value(this,1)"> +</span>                          <span class="sous_total">····</span> EUR   :</div></div>
 :   _________________________________________________   :
 :                                                       :
 :   <div class="checks" data-preis="10" data-quant="0"><input type="checkbox" class="checkinp" name="editions[]" id="exchange" value="Exchange Program #1 (10 EUR)"><label  for="exchange">Exchange Program #1</label>                                 :<div class="quantity_cont"><br> :   Qté      <span class="dec" onclick="incdec_value(this,-1)">- </span><input name="editionsqt[]" type="number" min="0" max="9" value="0"><span class="inc" onclick="incdec_value(this,1)"> +</span>                          <span class="sous_total">····</span> EUR   :</div></div>
 :   _________________________________________________   :
 :                                                       :
 :   <div class="checks" data-preis="300" data-quant="0"><input type="checkbox" class="checkinp" name="editions[]" id="papierpeinture" value="Papier Peinture / Sample Book (300 EUR)"><label  for="papierpeinture">Papier Peinture / Sample Book</label>                       :<div class="quantity_cont"><br> :   Qté      <span class="dec" onclick="incdec_value(this,-1)">- </span><input name="editionsqt[]" type="number" min="0" max="9" value="0"><span class="inc" onclick="incdec_value(this,1)"> +</span>                          <span class="sous_total">····</span> EUR   :</div></div>
 :   _________________________________________________   :
 :                                                       :
 :   <div class="checks" data-preis="5" data-quant="0"><input type="checkbox" class="checkinp" name="editions[]" id="tac3" value="tac3"><label for="tac3">Table as a curator / Autour de la table<span class="retourligne" >          :   <br> :      </span>#3 Mixing console / Table de mixage    70 EUR</label></div>    :
 :                                                       :
 :   <div class="checks" data-preis="5" data-quant="0"><input type="checkbox" class="checkinp" name="editions[]" id="tac2" value="tac2"><label for="tac2">Table as a curator / Autour de la table<span class="retourligne" >          :   <br> :      </span>#2 Printpress / Table de sérigraphie   15 EUR</label></div>    :
 :                                                       :
 :   <div class="checks" data-preis="5" data-quant="0"><input type="checkbox" class="checkinp" name="editions[]" id="tac1" value="tac1"><label for="tac1">Table as a curator / Autour de la table<span class="retourligne" >          :   <br> :      </span>Nightstand / Table de chevet           10 EUR</label></div>    :
 :                                                       :
 :                                                       :
 :. . . . . . . . . . . . . . . . . . . . . . . . . . . .:
 :                                                       :
 :    TOTAL (hors frais de port)                 <input type="text" name="total" readonly="true" class="textcoord" id="total" size="4"></span>    :
 :. . . . . . . . . . . . . . . . . . . . . . . . . . . .:
 :                                                       :
 :    Coordonnées                                        :
 :                                                       :
 :    E-MAIL *                                           :
 :    <input required   aria-required="true" placeholder="twolives@example.com" oninvalid="this.setCustomValidity('Rentrez une adresse mail valide. (ex : contact@exposerpublier.fr)')"   class="textcoord"type="text" name="email" id="email" size="47">    :
 :                                                       :
 :    NOM *                                              :
 :    <input required aria-required="true" placeholder="Devis" oninvalid="this.setCustomValidity('Rentrez votre nom.')"    class="textcoord"type="text" name="nom" id="nom" size="47">    :
 :                                                       :
 :    PR&Eacute;NOM *                                           :
 :    <input required  aria-required="true" placeholder="Benjamin" oninvalid="this.setCustomValidity('Rentrez votre prénom.')"   class="textcoord"type="text" name="prenom" id="prenom" size="47">    :
 :                                                       :
 :    ADRESSE *                                          :
 :    <input required  aria-required="true" placeholder="2 rue de la Révolution" oninvalid="this.setCustomValidity('Rentrez votre adresse de livraison (ex : 32bis Avenue du Docteur Fouchard).')"   class="textcoord"type="text" name="adresse" id="adresse" size="47">    :
 :                                                       :
 :    VILLE *                                            :
 :    <input required  aria-required="true" placeholder="Oberstein-le-Haut" oninvalid="this.setCustomValidity('Rentrez votre ville de livraison')"   class="textcoord"type="text" name="ville" id="ville" size="47">    :
 :                                                       :
 :    CODE POSTAL *                                      :
 :    <input required  aria-required="true" placeholder="68950" oninvalid="this.setCustomValidity('Rentrez un code postal')"    class="textcoord"type="text" name="codepostal" id="codepostal" size="47">    :
 :                                                       :
 :    ETAT                                               :
 :    <input class="textcoord"type="text" name="etat" placeholder="Ohio" id="etat" size="47">    :
 :                                                       :
 :    PAYS *                                             :
 :    <input required  aria-required="true" placeholder="France" oninvalid="this.setCustomValidity('Rentrez un pays')"   class="textcoord"type="text" name="pays" id="pays" size="47">    :
 :                                                       :
 :. . . . . . . . . . . . . . . . . . . . . . . . . . . .:
 :                                                       :
 :   En cliquant sur le bouton suivant, vous recevrez    :
 :   un e-mail de synthèse de commande. ExposerPublier   :
 :   recevra aussi votre bon de commande et vous         :
 :   et vous adressera alors dans les 5 jours ouvrés     :
 :   un e-mail de confirmation avec lien de paiement.    :
 :   En envoyant ce formulaire, vous acceptez les        :
 :   conditions générales de vente.                      :
 :                                                       :
 :                                                       :
 :                       {<input type="submit" value="ENVOYER">}                       :
 :                                                       :
 :. . . . . . . . . . . . . . . . . . . . . . . . . . . .:
 :                                                       :
 :                        MERCI !                        :
 :                                                       :
 :- - - - - - - - - - - - - - - - - - - - - - - - - - - -:

</form>

 ↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔↔

 <a href="credits.html">Crédits</a>                                            <a href="cgv.html">C.G.V.</a>


</body>
<script src="script.js" type="text/javascript"></script>

</html>
